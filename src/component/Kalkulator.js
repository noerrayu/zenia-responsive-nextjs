import React, { useState } from "react";
import NumberFormat from 'react-number-format';
import { Container, Row, Col, Card, InputGroup } from "react-bootstrap";

function Kalkulator() {
  const [usia, setUsia] = useState(0);
  const [penghasilan, setPenghasilan] = useState(0);
  const [cicilan, setCicilan] = useState(0);
  const [kemampuan, setKemampuan] = useState();
  const [harga, setHarga] = useState();
  const [dp, setDp] = useState();
  const [isOpened, setIsOpened] = useState(false);

  //handle input change
  const handleUsiaChange = (event) => setUsia(event.target.value);
  const handlePenghasilanChange = (event) => setPenghasilan(event.target.value);
  const handleCicilanChange = (event) => setCicilan(event.target.value);

  //perhitungan kalkulator Zenia
  const computeCalc = () => {
    let x = penghasilan * 0.3 - cicilan;
    setKemampuan(x);
    let y = (50 - usia) * 12 * x;
    setHarga(y);
    let z = 0.15 * y;
    setDp(z);
    setIsOpened(true);
  };

  const resetCalc = () => {
    setKemampuan(0);
    setHarga(0);
    setDp(0);
    setIsOpened(false);
  };

  return (
      <Container id="kalkulator"
      fluid className="container-fluid calculator" style={{marginBottom:"100px",backgroundImage: "url(" + `${require("../../public/calc-bg.png")}` + ")",
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover", backgroundSize:"550px 400px", backgroundPosition:"right",
    }}> 
      <Container>
        <Row>
          <Col xs={12} md={6}>
            <h3 style={{marginBottom:"25px" }}>
            Cari Tahu Kemampuan <br />
             <span>Menabung/Cicilanmu</span> 
            </h3>
            <form>
                      <h6>Umur Saat Ini</h6>
                      <InputGroup>
                      <input type='number' min='0' max='50' className='formControl' onChange={handleUsiaChange} />
                      <InputGroup.Prepend>
                        <InputGroup.Text id="basic-addon1" style={{height:"30px", backgroundColor:"#c3e1fd", border:"none"}}>Tahun</InputGroup.Text>
                        </InputGroup.Prepend>
                       </InputGroup>
                      <h6>Penghasilan Per-Bulan</h6>
                      <InputGroup>
                      <InputGroup.Prepend>
                        <InputGroup.Text id="basic-addon1" style={{height:"30px", backgroundColor:"#c3e1fd", border:"none"}}>Rp</InputGroup.Text>
                        </InputGroup.Prepend>
                        <input type='number' min='0' max='2000000000' className='formControl' onChange={handlePenghasilanChange} />
                       </InputGroup>
                      <h6>Cicilan Lainya</h6>
                      <InputGroup>
                      <InputGroup.Prepend>
                        <InputGroup.Text id="basic-addon1" style={{height:"30px", backgroundColor:"#c3e1fd", border:"none"}}>Rp</InputGroup.Text>
                        </InputGroup.Prepend>
                        <input type='number' min='0' max='2000000000' className='formControl' onChange={handleCicilanChange} />
                       </InputGroup>
                      {!isOpened && (
                      <button onClick={computeCalc} value='Submit' style={{color:"#fff", backgroundColor:"#3ba1ff", border:"none"}}>Hitung</button>
                      )}
                      {isOpened && (
                      <button onClick={resetCalc} value='Submit' style={{color:"#3ba1ff", backgroundColor:"#fff", borderColor:"#3ba1ff"}}><span style={{color: "#3BA1FF"}}>Reset</span></button>
                      )}
                    </form>
          </Col>
          <Col xs={12} md={6}>
            {!isOpened && (
                <img src="/city.png" width="350px" style={{float:"right"}}/>     
            )}
            {isOpened && (
              <Card style={{boxShadow: "0px 2px 10px rgba(59, 161, 255, 0.25)", marginTop:"20px"}}>
              <Card.Header style={{backgroundColor:"#4643D3", color:"#fff", fontWeight:"700"}}>Kemampuan menabung/cicilanmu per-bulan <br /> sebesar: <h3 style={{marginTop:"20px"}}>Rp. <NumberFormat style={{color:"#fff"}} value={kemampuan} displayType={'text'} thousandSeparator={true} /></h3></Card.Header>
              <Card.Body mb-4>
             <h6> Bersama Zenia kamu bisa beli rumah hingga<br />harga:</h6>
             <h3 style={{marginTop:"20px"}}>Rp. <NumberFormat style={{color:"#000"}} value={harga} displayType={'text'} thousandSeparator={true} /></h3>
             <p style={{color:"#888888", fontWeight:"700"}}>DP mulai dari : <NumberFormat style={{color:"#000",fontWeight:"700"}} prefix="Rp. " value={dp} displayType={'text'} thousandSeparator={true} /></p>
             <h5 style={{fontWeight:"bold"}}>Download Zenia Sekarang</h5>
             <a href="https://play.google.com/" target="_blank"> <img src="/googleplay.png" width="192px" /></a>
              </Card.Body>
            </Card>
            )}
          </Col>
        </Row>
        </Container>
      </Container>
  );
}

export default Kalkulator;
