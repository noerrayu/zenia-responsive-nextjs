import React from 'react'
import { Container, Row, Col} from "react-bootstrap";

function hunian() {
    return (
        <Container fluid className="container-fluid hunian" style={{backgroundColor:"#f0f8ff",paddingTop:"100px",paddingBottom:"100px"}}>
        <Container>
        <h3 style={{textAlign:"center", alignSelf:"center", marginBottom:"60px"}}>4 Langkah Mudah <span>Memiliki Hunian</span></h3>
        <Row style={{textAlign:"center", alignSelf:"center"}}>
          <Col  xs={12} md={3}><div className="card-hunian"><img src="/kpr-1.png" width="261px"/><h4 style={{color:"000",fontWeight:"bold",fontSize:"20px",marginTop:"40px"}}>Mengisi
   Form Pengajuan KPR</h4><h6>Ajukan KPR secara online kapanpun tanpa ribet</h6></div></Col>
          <Col  xs={12} md={3}><div className="card-hunian"><img src="/kpr-2.png" width="261px"/><h4 style={{color:"000",fontWeight:"bold",fontSize:"20px",marginTop:"40px"}}>Mengikuti 
   Proses Wawancara</h4><h6>Proses wawancara dapat dilakukan secara online</h6></div></Col>
          <Col  xs={12} md={3}><div className="card-hunian"><img src="/kpr-3.png" width="261px"/><h4 style={{color:"000",fontWeight:"bold",fontSize:"20px",marginTop:"40px"}}>Proses DP dan
   Pembayaran Cicilan</h4><h6>Bayar DP minimal 15% dan mulai bayar cicilan KPR-mu</h6></div></Col>
          <Col  xs={12} md={3}><div className="card-hunian"><img src="/kpr-4.png" width="261px" /><h4 style={{color:"000",fontWeight:"bold",fontSize:"20px", marginTop:"40px"}}>Miliki<br />
   Hunian Impian Kamu</h4><h6>Selesaikan cicilan dan miliki hunian impian kamu</h6></div></Col>
        </Row>
        </Container>
        </Container>
    )
}

export default hunian
