import React from "react";
import { Container, Navbar, Nav, Button } from "react-bootstrap";

function navbar() {
  return (
    
      <Navbar sticky="top" bg="light" expand="lg" style={{  boxShadow: "0px 2px 10px rgba(59, 161, 255, 0.25)"}}>
        <Container className="container-fluid" >
        <Navbar.Brand href="#home">
          <img src="/logo-zenia.png" width="105px" />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="ml-auto" defaultActiveKey="/">
            <Nav.Link className="mr-3 " href="/">
              Beranda
            </Nav.Link>
            <Nav.Link className="mr-3" href="#kalkulator" eventKey="link-1">
              Kalkulator
            </Nav.Link>
            <Nav.Link className="mr-3" href="#tabungan" eventKey="link-2">
              Tabungan
            </Nav.Link>
            <Nav.Link className="mr-3" href="#kpr" eventKey="link-3">
              KPR
            </Nav.Link>
            <Nav.Link className="mr-3" href="#tentang" eventKey="link-4">
              Tentang
            </Nav.Link>
         
            <Button variant="outline-primary" style={{fontSize:"14px", fontWeight:"600px"}} >DOWNLOAD</Button>
          
          </Nav>
        </Navbar.Collapse>
        </Container>
      </Navbar>
   
  );
}

export default navbar;
