import React from 'react'
import { Container, Row, Col, Media} from "react-bootstrap";

export default function Footer() {
  return (
    <Container  id="tentang" fluid className="container-fluid footer" style={{backgroundColor:"#3ba1ff", paddingTop:"64px", paddingBottom:"83px", marginTop:"83px"}}>
     <Container>
     <Row>
       <Col><img className="footer-logozenia" src="zenia-footer.png" width="356px" style={{marginBottom:"24px"}} /><p>Zenia adalah aplikasi mobile banking
yang membantu milenial untuk merencanakan
kepemilikan hunian impian tanpa ribet</p></Col>
       <Col><p style={{fontSize:"16px", fontWeight:"bold", color:"#fff",marginBottom:"24px"}}>Hubungi Kami</p><a href="//google.com/maps/search/Jalan+Tompeyan+CT+VIII+Tegal+Rejo,+Yogyakarta+DI+Yogyakarta/@-7.7829994,110.3480592,15z/data=!3m1!4b1" target="_blank"><p>Jalan Tompeyan
CT VIII Tegal Rejo, Yogyakarta
DI Yogyakarta</p></a>
<Media>
  <img
    width={16}
    className="mr-3"
    src="/icon-telp.png"
  />
  <Media.Body>
    <a href="tel:+62852-2366-8417"> <p>
    +62 85223668417
    </p></a>
  </Media.Body>
</Media>
<Media>
  <img
    width={16}
    className="mr-3"
    src="/icon-msg.png"
  />
  <Media.Body>
  <a href = "mailto: zeniaindonesia@gmail.com" target="_blank"><p>zeniaindonesia@gmail.com</p></a>
  </Media.Body>
</Media>
</Col>
       <Col><p style={{fontSize:"16px", fontWeight:"bold", color:"#fff",marginBottom:"24px"}}>Follow Sosial Media Kami</p>
       <div className="link-socialmedia">
         <a href='//instagram.com/zenia.indonesia/'
              target='_blank'><img src="/fb.png"/></a>
         <a href='//instagram.com/zenia.indonesia/'
              target='_blank' style={{marginLeft:"16px"}}><img src="/ig.png"/></a>
         <a href='//instagram.com/zenia.indonesia/'
              target='_blank' style={{marginLeft:"16px"}}><img src="/twitter.png"/></a>
       </div>
       <p style={{fontSize:"16px", fontWeight:"bold", color:"#fff",marginTop:"32px"}}>Download Sekarang</p>
       <a href='//play.google.com/'
              target='_blank'><img src="/googleplay.png" width="110px"/></a>
       </Col>
     </Row>
     </Container>
   </Container>
  )
}
