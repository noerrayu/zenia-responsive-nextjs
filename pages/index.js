import Head from "next/head";
import { Container, Row, Col, Carousel } from "react-bootstrap";

import Navbar from "../src/component/Navbar";
import Tabungan from "../src/component/Tabungan";
import Kalkulator from "../src/component/Kalkulator";
import KPR from "../src/component/KPR";
import BannerSlider from "../src/component/BannerSlider";
import ServiceProgram from "../src/component/ServiceProgram";
import Hunian from "../src/component/Hunian";
import Download from "../src/component/Download";
import Footer from "../src/component/Footer";
import Copyright from "../src/component/Copyright";

const Home = () => (
  <>
    <Head>
      <title>Create Next App</title>
      <link rel="icon" href="/favicon.ico" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>

    <Navbar />
    <BannerSlider />
    <ServiceProgram />
    <Kalkulator />
    <Tabungan />
    <KPR />
    <Hunian />
    <Download />
    <Footer />
  <Copyright />
  </>
);

export default Home;
